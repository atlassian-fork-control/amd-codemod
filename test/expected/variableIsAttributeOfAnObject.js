define(
    "test/input/variableIsAttributeOfAnObject",
    ["what/the/foo"],
    function(wtf) {
        wtf.bar();

        this.whatthefoo.bar();

        woot.whatthefoo.bar();
    }
);
