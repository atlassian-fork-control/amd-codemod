define("test/input/requireStrategyWithUseStrict", ["require"], function(require) {
    "use strict";
    var foo = require("f/o/o");
    foo.bar();
});
