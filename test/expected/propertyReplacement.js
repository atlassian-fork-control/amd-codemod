define("test/property-replacement", ["require"], function(require) {
    var BA = require("foobar/bar");
    var BAR = require("foobar/bar/baz");
    var TheRest = require("module/the-rest");
    var Second = require("module/2");
    var First = require("module/1");
    var handler = BAR.baz;
    First.bind("ready", handler);

    if (BA.R.foo === true) {
        TheRest.one("click", handler);
    }

    Second.trigger("ready");
});
