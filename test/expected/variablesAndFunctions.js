define("test/input/variablesAndFunctions", ["baz/foo", "bar/baz", "foo/bar", "b/a/z", "b/a/r", "f/o/o"], function(Bazfoo, Barbaz, foobar, baz, bar, foo) {
    if(foo.bar.z() && bar.baz) {
        var x = baz.test();
        foobar(bar.baz, x, true);
    } else {
        Bazfoo(bar, baz);
    }

    Barbaz("test");
});
