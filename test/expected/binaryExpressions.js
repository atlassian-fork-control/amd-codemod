define("test/binary-expressions", ["foobar/barbaz", "bar/baz", "foobar/foofoo", "jquery", "some/dep"], function(baz, Barbaz, FOO, jQuery, somedep) {
    var check1 = jQuery("whatever") instanceof jQuery;
    var check2 = somedep instanceof FOO.bar("whatever");
    var check3 = baz === Barbaz;
    var check4 = typeof FOO.bar === 'function';
});
