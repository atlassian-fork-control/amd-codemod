define("test/input/localVariables", [], function() {
    var foo = {bar: {baz: function() {return true}}, baz: false};

    foo.bar.baz();
});
