"use strict";

module.exports = function(fileInfo, api) {
    var jscodeshift = api.jscodeshift;

    var amd = require("../../index")(jscodeshift);

    var src = jscodeshift(fileInfo.source);

    var allCalls = src.getAMDCalls();

    allCalls.forEach(function(path) {
        var code = jscodeshift(path);

        return code
            .toAMD("$", "jquery", "jQuery")
            .toAMD("foo", "f/o/o")
            .toAMD("bar", "b/a/r")
            .toAMD("baz", "b/a/z")
            .toAMD("whatthefoo", "what/the/foo", "wtf")
            .toAMD("A.B.C", "a/b/c", "ABC")
            .toAMD("F.O.O", "foobar/foofoo", "FOO")
            .toAMD("JIRA.Issues.Brace", "jira/components/libs/brace", "Brace")
            .toAMD("foobar", "foo/bar")
            .toAMD("barbaz", "bar/baz", "Barbaz")
            .toAMD("bazfoo", "baz/foo", "Bazfoo")
            .toAMD("B.A.R.baz", "foobar/barbaz", "baz")
            .toAMD("Global#oldMethod", "modules/methodRenaming", "methodRenaming", "newMethod")
            .toSource();
    });

    return src.toSource();
};
