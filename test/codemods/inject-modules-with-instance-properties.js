"use strict";
module.exports = function(fileInfo, api) {
    var jscodeshift = api.jscodeshift;

    var amd = require('../../index')(jscodeshift);

    var src = jscodeshift(fileInfo.source);

    return src
        .getAMDDefineCalls()
        .forEach(function(path) {
            var code = jscodeshift(path);

            return code
                .toAMD("Namespace#bind", "module/1", "First")
                .toAMD("Namespace#trigger", "module/2", "Second")
                .toAMD("Namespace", "module/the-rest", "TheRest")
                .toAMD("B.A.R#baz", "foobar/bar/baz", "BAR")
                .toAMD("B.A", "foobar/bar", "BA")
                .toSource();
        })
        .toSource();
};
