"use strict";
const fs = require('fs');
const path = require('path');
const jscodeshiftRunner = require('jscodeshift/dist/Runner');
const expect = require("chai").use(require('chai-as-promised')).expect;

const input = "test/input/";
const actual = "test/actual/";
const expected = "test/expected/";

function runJSCodeshift(transform, outputPath) {
    const opts ={
        extensions: 'js',
        runInBand: true,
        verbose: false,
        silent: true
    };
    opts.transform = path.resolve(transform);
    opts.path = [outputPath];

    return jscodeshiftRunner.run(
        opts.transform,
        opts.path,
        opts
    );
}

module.exports = function(codemod) {
    return function assertTransform(fileName) {
        const inputFile = path.resolve(path.join(input, fileName));
        const outputFile = path.resolve(path.join(actual, fileName));
        const expectedFile = path.resolve(path.join(expected, fileName));
        const codemodFilePath = path.resolve(path.join(__dirname, '..', codemod));

        fs.writeFileSync(outputFile, fs.readFileSync(inputFile));

        return runJSCodeshift(codemodFilePath, outputFile)
            .should.be.fulfilled
            .then(function(result) {
                expect(result).to.have.property("error", 0);
                expect(fs.readFileSync(outputFile, 'utf8')).to.equal(fs.readFileSync(expectedFile, 'utf8'), fileName);
            });
    }
}
