define('test/data/variables', [], function() {
    foo.bar();
    foo.baz();

    function innerfunc() {
        bar.baz(foo);

        return baz.toString();
    }

    innerfunc();
});