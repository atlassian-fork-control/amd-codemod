define("test/input/variablesAndFunctions", [], function() {
    if(foo.bar.z() && bar.baz) {
        var x = baz.test();
        foobar(bar.baz, x, true);
    } else {
        bazfoo(bar, baz);
    }

    barbaz("test");
});
