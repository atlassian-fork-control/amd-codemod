"use strict";
const assertTransformFactory = require('./utils/assert-transform.js');
const cleanup = require('./utils/cleanup.js');

describe("Transform: to ES6 modules", function() {
    after(cleanup);

    const assertTransform = assertTransformFactory("../lib/codemods/to-es6-modules.js");

    it("should convert to a default export statement", () => assertTransform("defaultExport.js"));
    it("should convert exports module to a constant", () => assertTransform("exportConst.js"));
    it("replaces require.ensure with System.import", () => assertTransform("systemImport.js"));
});
