"use strict";
const types = require("ast-types").namedTypes;

module.exports = function(j, node) {
    const collection = j(node).find(j.BlockStatement);
    if(!collection.size()) {
        throw new Error('Node has no body.');
    }
    const body = collection.nodes()[0];

    function isUseStrict(collection) {
        return collection.findUseStrict().size() > 0;
    }

    function isAMDRequireCall(collection) {
        return collection.getAMDRequireCalls(true).size() > 0;
    }

    function createRequireCall(moduleName) {
        return j.callExpression(
            j.identifier("require"),
            [j.literal(moduleName)]
        )
    }

    function createModuleDeclaration(varName, moduleName) {
        if (!moduleName) {
            return j(j.expressionStatement(createRequireCall(varName))).toSource({wrapColumn: Infinity});
        }
       
        return j.variableDeclaration("var", [
            j.variableDeclarator(
                j.identifier(varName),
                createRequireCall(moduleName)
            )
        ]);
    }

    function getDepenencies() {
        return j(body)
            .find(j.VariableDeclarator)
            .where(path => path.getAMDRequireCalls(true));
    }

    function getDependenciesByModuleName(moduleName) {
        return getDepenencies()
            .where(path => path
                .getAMDRequireCalls(true)
                .filter(path => {
                    var node = path.value;
                    return node.arguments
                        && node.arguments[0]
                        && node.arguments[0].value === moduleName;
                })
            );
    }

    function addDependency(varName, moduleName) {
        const candidate = j(body).find(j.Statement).findFirst(statement => {
            return !isUseStrict(statement);
        })
        candidate.insertBefore(createModuleDeclaration(varName, moduleName));
    }

    function hasDependency(moduleName) {
        return getDependenciesByModuleName(moduleName).size() > 0;
    }

    function getDependency(moduleName) {
        const dep = getDependenciesByModuleName(moduleName);
        if (dep.size() == 0) throw new Error("Could not find name for dependency.");

        return dep.nodes()[0].id.name;
    }

    function removeDependencies() {
        getDepenencies().forEach(dep => dep.prune());
    }

    return {
        addDependency,
        hasDependency,
        getDependency,

        getDepenencies,
        removeDependencies
    }
}
